package com.dh.fullstack.users.service.controller;

import com.dh.fullstack.users.service.bean.Asus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;


@RestController
@RequestMapping("/beans")
@RequestScope
public class BeanTestController {

    private Integer value = 1;
    @Autowired
    private Asus asus;

//    public Asus getAsus() {
//        return asus;
//    }
//
//    //Inyeccion por el metodo Seter
//    @Autowired
//    public void setAsus(Asus asus) {
//        this.asus = asus;
//    }

//    public AccountController(Asus asus) {
//
//        this.asus = asus;
//    }

    //    public AccountController() {
//        this.asus = new Asus(); NO usar jamas
//    }

    @RequestMapping(value = "/asus", method = RequestMethod.GET)
    public Asus redAsus(){
        value = value +1;
        asus.setName(asus.getName() + ": GET"+ ", Value: " +value);
        return asus;
    }
}
