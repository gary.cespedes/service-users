package com.dh.fullstack.users.service.config;

import com.dh.fullstack.users.service.bean.Asus;
import com.dh.fullstack.users.service.bean.Persona;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;


@Configuration
public class Config {

    @Bean
    @Scope("prototype")
    public Asus beanAsus(){
        Asus asus = new Asus();
        asus.setName("I am asus");

        return asus;
    }

    @Bean
    @Scope("prototype")
    public Persona beanPersona(){
        Persona persona = new Persona();
        persona.setName("Yo soy Gary");
        return persona;
    }
}
