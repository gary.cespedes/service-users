package com.dh.fullstack.users.service.controller;

import com.dh.fullstack.users.service.bean.Persona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Gary A. Cespedes
 **/

@RestController
@RequestMapping(value = "/beansPersona")
@RequestScope
public class BeanPersonaController {
    private Integer value = 1;
    @Autowired
    private Persona persona;


    @RequestMapping(value = "/personas", method = RequestMethod.GET)
    public Persona readPersona(){
        value = value + 1;
        persona.setName("German");
        return persona;
    }

}
